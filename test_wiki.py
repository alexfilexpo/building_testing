import pytest
from selenium.webdriver import Chrome
from page import MainWikiPage
from locators import MainWikiPageLocators as locators


class TestWikiBuild(object):

    @pytest.fixture(scope="class")
    def browser(self):
        driver = Chrome('./chromedriver')
        driver.implicitly_wait(10)
        wiki_page = MainWikiPage(driver)
        wiki_page.page_load()
        yield wiki_page
        driver.quit()

    def test_wiki_buildings_table(self, browser):
        table = browser.get_table()
        assert table is not None, "There is no table on a page"

    def test_table_columns(self, browser):
        table = browser.get_table()
        columns_names = browser.get_col_names(table)
        assert len(columns_names) == 10, "No column names were found"

    def test_desc_sorting(self, browser):
        table = browser.get_table()
        default_col_data = browser.get_col_data(table, locators.HEIGHT_FT)
        sorted_default_data = sorted(default_col_data)
        browser.table_elem_click(table, "ft")
        sorted_col_page_data = browser.get_col_data(table, locators.HEIGHT_FT)
        assert sorted_col_page_data == sorted_default_data, "Column descending sorting not functioning"

    def test_asc_sorting(self, browser):
        table = browser.get_table()
        default_col_data = browser.get_col_data(table, locators.YEAR)
        sorted_default_data = sorted(default_col_data, reverse=True)
        browser.table_elem_click(table, "Year")
        browser.table_elem_click(table, "Year")
        sorted_col_page_data = browser.get_col_data(table, locators.YEAR)
        assert sorted_col_page_data == sorted_default_data, "Column ascending sorting not functioning"

    def test_oldest_build_capture(self, browser):
        browser.page_load()
        table = browser.get_table()
        name = browser.get_oldest_build_name(table)
        assert name == "Empire State Building", "The oldest building name captured wrong"

    def test_country_most_built(self, browser):
        table = browser.get_table()
        most_country = browser.country_most_build(table)
        assert most_country == "China"