# Test project for "List of tallest buildings" Wiki page

#### To run it, you should: 
```sh
- activate virtual environment
- install all requied packages from requrements.txt
- run "python -m pytest ." command inside "building_testing" directory
```