from collections import Counter
from locators import MainWikiPageLocators as Locators

class MainWikiPage:
    URL = 'https://en.wikipedia.org/wiki/List_of_tallest_buildings'

    def __init__(self, browser):
        self.browser = browser
        self.locators = Locators()

    def page_load(self):
        self.browser.get(self.URL)

    def table_elem_click(self, table_elem, elem_title):
        table_elem.find_element(*self.locators.TABLE_ELEM_CLICK(elem_title)).click()

    def get_table(self):
        return self.browser.find_element(*self.locators.WIKI_TABLE)

    def get_col_names(self, table_elem):
        names = []
        col_names_row = table_elem.find_elements(*self.locators.COL_NAMES_ROW)
        for col_title in col_names_row:
            if col_title.text == 'Height[9]':
                continue
            if col_title.text == 'm':
                col_name = f"Height ('{col_title.text}')"
                names.insert(5, col_name)
            elif col_title.text == 'ft':
                col_name = f"Height ('{col_title.text}')"
                names.insert(6, col_name)
            else:
                names.append(col_title.text)
        return names

    def get_col_data(self, table_elem, col_place_num):
        all_table_rows = table_elem.find_elements(*self.locators.TABLE_ROWS)
        col_data = []
        for row in all_table_rows:
            count = len(row.find_elements(*self.locators.ROW_ELEMENTS))
            if count < 10:
                if count == 7:
                    col_index = col_place_num-2
                else:
                    col_index = col_place_num-1
            else:
                col_index = col_place_num
            col_data.append(row.find_element(*self.locators.ROW_COL_CELL(col_index)).text)
        return col_data

    def get_oldest_build_name(self, table_elem):
        self.table_elem_click(table_elem, "Year")
        build_name = table_elem.find_element(*self.locators.BUILD_NAME).get_attribute('title')
        return build_name

    def country_most_build(self, table_elem):
        countries_data = self.get_col_data(table_elem, self.locators.COUNTRY)
        build_amount_per_country = Counter(countries_data)
        country = ""
        builds = 0
        for k, v in build_amount_per_country.most_common():
            if v > builds:
                country = k.strip()
                builds = v
        return country
