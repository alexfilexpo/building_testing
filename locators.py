from selenium.webdriver.common.by import By

class MainWikiPageLocators(object):
    # Table column titles indexes
    RANK = 1
    NAME = 2
    IMAGE = 3
    CITY = 4
    COUNTRY = 5
    HEIGHT_M = 6
    HEIGHT_FT = 7
    FLOORS = 8
    YEAR = 9
    NOTES = 10

    # XPath table variables
    WIKI_TABLE = (By.XPATH, '//table[thead/tr/th/text()[contains(.,"Image")]]')
    COL_NAMES_ROW = (By.XPATH, './thead/tr/th')
    TABLE_ROWS = (By.XPATH, './tbody/tr')
    ROW_ELEMENTS = (By.TAG_NAME, 'td')
    ROW_COL_CELL = (By.XPATH, "./td[%d]")
    BUILD_NAME = (By.XPATH, "./tbody/tr[1]/td[2]/b/a")

    def TABLE_ELEM_CLICK(self, elem_title):
        xpath = f"./thead/tr/th[contains(., '%s')]" % elem_title
        return (By.XPATH, xpath)

    def ROW_COL_CELL(self, col_index):
        xpath = f"./td[%d]" % col_index
        return (By.XPATH, xpath)